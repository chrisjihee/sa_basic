/**
 * 
 */
package util;

import java.util.ArrayList;

/**
 * List를 표현하고 처리하는 클래스
 * 
 * @author  Jihee Ryu
 * @since   2012/10/07
 * @version 2012/10/07
 */
public class ZList {
	
	public static final long serialVersionUID = 0x5000000000000407L;

	protected String sText = new String();

	/**
	 * Main function
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
	}
	
	/**
	 * 
	 * @param a
	 * @return
	 */
	public static String[] toStringArray(Object[] a) {
		String[] b = new String[a.length];
		for(int i=0; i<a.length; i++)
			b[i] = a[i].toString();
		return b;
	}
	
	
	/**
	 * 
	 * @param sNeedle
	 * @return
	 */
	public static int countElementsContains(ArrayList<String> lstItem, String sNeedle) {
		int nCount = 0;
		for(String sItem: lstItem)
			if(sItem.contains(sNeedle))
				nCount++;
		return nCount;
	}
	
	/**
	 * 
	 * @param sRegEx
	 * @return
	 */
	public static int countElementsMatches(ArrayList<String> lstItem, String sRegEx) {
		int nCount = 0;
		for(String sItem: lstItem)
			if(sItem.matches(sRegEx))
				nCount++;
		return nCount;
	}
	
	/**
	 * 
	 * @param sNeedle
	 * @return
	 */
	public static ArrayList<String> filterElementsContains(ArrayList<String> lstItem, String sNeedle) {
		for(int i=0; i<lstItem.size(); i++) {
			if(lstItem.get(i).contains(sNeedle)) {
				lstItem.remove(i);
				i--;
			}
		}
		return lstItem;
	}
	
	/**
	 * 
	 * @param sNeedle
	 * @return
	 */
	public static ArrayList<String> filterElementsNotContains(ArrayList<String> lstItem, String sNeedle) {
		for(int i=0; i<lstItem.size(); i++) {
			if(!lstItem.get(i).contains(sNeedle)) {
				lstItem.remove(i);
				i--;
			}
		}
		return lstItem;
	}
	
	/**
	 * 
	 * @param sRegEx
	 * @return
	 */
	public static ArrayList<String> filterElementsMatches(ArrayList<String> lstItem, String sRegEx) {
		for(int i=0; i<lstItem.size(); i++) {
			if(lstItem.get(i).matches(sRegEx)) {
				lstItem.remove(i);
				i--;
			}
		}
		return lstItem;
	}
	
	/**
	 * 
	 * @param sRegEx
	 * @return
	 */
	public static ArrayList<String> filterElementsNotMatches(ArrayList<String> lstItem, String sRegEx) {
		for(int i=0; i<lstItem.size(); i++) {
			if(!lstItem.get(i).matches(sRegEx)) {
				lstItem.remove(i);
				i--;
			}
		}
		return lstItem;
	}
}
