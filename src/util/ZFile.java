/**
 * 입출력에 관련된 패키지
 */
package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * 파일을 처리하기 위한 클래스
 * 
 * @author Jihee Ryu
 */
public class ZFile implements Serializable {
	
	public static final long serialVersionUID = 0x5000000000000102L;

	public static final int TO_READ      = 0x4021;
	public static final int TO_WRITE     = 0x4022;
	public static final int TO_APPEND    = 0x4023;

	public static final char   SEP_DIR    = File.separatorChar;
	public static final String SEP_DIRS   = String.valueOf(SEP_DIR);
	public static final String REG_SEP_DIR = String.valueOf(SEP_DIR).replaceAll("\\\\", "\\\\\\\\");
	public static final String SEP_LINE   = "\r\n";
	public static final String REG_LINE   = "\r?\n";

	public static final String STR_PREFIX_FOR_RESERVED_NAME = "__RSV__";
	public static final String[] STR_RESERVED_NAME = { "con" };

	public static final String DEF_CHARSET = "UTF-8";

	private FileInputStream    oInpStream;
	private InputStreamReader  oInpReader;
	private FileOutputStream   oOutStream;
	private OutputStreamWriter oOutWriter;

	protected ZFile() {
		
	}

	/**
	 * Constructor Function
	 * @param fileTarget
	 * @param nType
	 */
	public ZFile(String fileTarget, int nType) {
		this(fileTarget, nType, DEF_CHARSET);
	}

	/**
	 * Constructor Function
	 * @param fileTarget
	 * @param nType
	 * @param sCharset
	 */
	public ZFile(String fileTarget, int nType, String sCharset) {
		
		try {
			switch(nType) {
				// 읽기용
				case TO_READ:
					oInpStream = new FileInputStream(fileTarget);
					oInpReader = new InputStreamReader(oInpStream, sCharset);
					oOutStream = null;
					oOutWriter = null;
					break;
				// 쓰기용
				case TO_WRITE:
					oInpStream = null;
					oInpReader = null;
					oOutStream = new FileOutputStream(fileTarget);
					oOutWriter = new OutputStreamWriter(oOutStream, sCharset);
					break;
				// 이어 쓰기용
				case TO_APPEND:
					oInpStream = null;
					oInpReader = null;
					oOutStream = new FileOutputStream(fileTarget, true);
					oOutWriter = new OutputStreamWriter(oOutStream, sCharset);
					break;
				default:
			}
		}
		catch(Exception e) {
		}
	}

	/**
	 * 파일을 닫는 함수
	 */
	public void close() {
		
		try {
			if(oInpReader != null)
				oInpReader.close();
			if(oOutWriter != null)
				oOutWriter.close();
		}
		catch(Exception e) {
		}
	}

	/**
	 * 파일 한 줄의 내용을 읽는 함수
	 * @return
	 */
	public String readLine() {
		
		String sLine = null;

		try {
			// 버퍼 채우기
			int c=0;
			StringBuffer buf = new StringBuffer();
			
			for(c=oInpReader.read(); c!=-1 && c!='\n'; c=oInpReader.read()) {
				if(c=='\r')
					continue;
				buf.append((char)c);
			}

			// 버퍼 리턴하기
			if(c!=-1 || buf.length()>0)
				sLine = buf.toString();
		}
		catch(Exception e) {
		}

		return sLine;
	}

	/**
	 * 파일의 나머지 내용을 한번에 읽는 함수
	 * @return
	 */
	public String readRemains() {
		
		String sCont = null;

		try {
			int c=0;
			StringBuffer buf = new StringBuffer();

			// 버퍼 채우기
			for(c=oInpReader.read(); c!=-1; c=oInpReader.read()) {
				buf.append((char)c);
			}

			// 버퍼 리턴하기
			if(c!=-1 || buf.length()>0)
				sCont = buf.toString();
		}
		catch(Exception e) {
		}

		return sCont;
	}

	/**
	 * 파일에 버퍼의 내용을 쓰는 함수
	 * @param sCont
	 */
	public void write(String sCont) {
		
		sCont = sCont.replaceAll(REG_LINE,"\n").replaceAll("\n",SEP_LINE);

		try {
			oOutWriter.write(sCont);
			oOutWriter.flush();
		}
		catch(Exception e) {
		}
	}

	/**
	 * 파일에 한 줄의 내용을 쓰는 함수
	 * @param buf
	 */
	public void writeLine(String buf) {
		
		write(buf+SEP_LINE);
	}

	/**
	 * 파일 내용을 바이트 단위로 쓰는 함수
	 * @param buf
	 */
	public void writeBytes(byte[] buf) {
		
		try {
			// 파일 채우기
			oOutStream.write(buf);
		}
		catch(Exception e) {
		}
	}

	/**
	 * 파일이나 디렉토리의 존재 여부를 리턴하는 함수
	 * @param fileTarget
	 * @return
	 */
	public static boolean exists(String fileTarget) {
		
		boolean bExist = false;

		try {
			File oFile = new File(fileTarget);
			bExist = oFile.exists();
		}
		catch(Exception e) {
		}

		return bExist;
	}
	
	/**
	 * 디렉토리 여부를 리턴하는 함수
	 * @param fileTarget
	 * @return
	 */
	public static boolean isDir(String fileTarget) {
		
		boolean bDir = false;

		try {
			File oFile = new File(fileTarget);
			bDir = oFile.isDirectory();
		}
		catch(Exception e) {
		}

		return bDir;
	}
	
	/**
	 * 파일 여부를 리턴하는 함수
	 * @param fileTarget
	 * @return
	 */
	public static boolean isFile(String fileTarget) {
		
		boolean bFile = false;

		try {
			File oFile = new File(fileTarget);
			bFile = oFile.isFile();
		}
		catch(Exception e) {
		}

		return bFile;
	}
	
	/**
	 * 숨김 속성 여부를 리턴하는 함수
	 * @param fileTarget
	 * @return
	 */
	public static boolean isHidden(String fileTarget) {
		
		boolean bHidden = false;

		try {
			File oFile = new File(fileTarget);
			bHidden = oFile.isHidden();
		}
		catch(Exception e) {
		}

		return bHidden;
	}

	/**
	 * 파일의 크기를 리턴하는 함수
	 * @param fileTarget
	 * @return
	 */
	public static int size(String fileTarget) {
		
		int nSize = -1;

		try {
			File oFile = new File(fileTarget);
			nSize = (int)oFile.length();
		}
		catch(Exception e) {
		}

		return nSize;
	}

	/**
	 * 파일 경로에서 파일 경로가 될 수 없는 특수문자들을 제거한다.
	 * @param sPath
	 * @return
	 */
	public static String stripSpecialAndReserved(String sPath) {

		// 특별 기호 제거하기
		sPath = ZFile.stripSpecialChar(sPath);
		
		// 예약어 제거하기
		sPath = ZFile.stripReservedWord(sPath);
		
		return sPath;
	}
	
	/**
	 * 파일 경로에서 파일 경로가 될 수 없는 특수문자들을 제거한다.
	 * @param sPath
	 * @return
	 */
	public static String stripSpecialAndReserved(String sPath, String sRepl) {

		// 특별 기호 제거하기
		sPath = ZFile.stripSpecialChar(sPath, sRepl);
		
		// 예약어 제거하기
		sPath = ZFile.stripReservedWord(sPath, sRepl);
		
		return sPath;
	}
	
	/**
	 * 파일 경로에서 파일 경로가 될 수 없는 특수문자들을 제거한다.
	 * @param sPath
	 * @return
	 */
	public static String stripSpecialChar(String sPath, String sRepl) {
		
		// 특별 기호 제거하기
		sPath = sPath.replace("\\", sRepl);
		sPath = sPath.replace("/", sRepl);
		sPath = sPath.replace(":", sRepl);
		sPath = sPath.replace("*", sRepl);
		sPath = sPath.replace("?", sRepl);
		sPath = sPath.replace("\"", sRepl);
		sPath = sPath.replace("<", sRepl);
		sPath = sPath.replace(">", sRepl);
		sPath = sPath.replace("|", sRepl);

		return sPath;
	}
	
	/**
	 * 파일 경로에서 파일 경로가 될 수 없는 특수문자들을 제거한다.
	 * @param sPath
	 * @return
	 */
	public static String stripSpecialChar(String sPath) {
		
		// 특별 기호 제거하기
		sPath = sPath.replace("\\", "＼");
		sPath = sPath.replace("/", "／");
		sPath = sPath.replace(":", "：");
		sPath = sPath.replace("*", "＊");
		sPath = sPath.replace("?", "？");
		sPath = sPath.replace("\"", "〃");
		sPath = sPath.replace("<", "＜");
		sPath = sPath.replace(">", "＞");
		sPath = sPath.replace("|", "｜");
		
		return sPath;
	}
	
	/**
	 * 파일 경로에서 파일 경로가 될 수 없는 예약어들을 제거한다.
	 * @param sPath
	 * @return
	 */
	public static String stripReservedWord(String sPath, String sRepl) {

		// 예약어 제거하기
		sPath = sPath.replaceAll("(^|/|\\\\)"+"con"+"([.]|/|\\\\|$)", sRepl);
		sPath = sPath.replaceAll("(^|/|\\\\)"+"prn"+"([.]|/|\\\\|$)", sRepl);
		sPath = sPath.replaceAll("(^|/|\\\\)"+"nul"+"([.]|/|\\\\|$)", sRepl);
		sPath = sPath.replaceAll("(^|/|\\\\)"+"aux"+"([.]|/|\\\\|$)", sRepl);
		
		return sPath;
	}
	
	/**
	 * 파일 경로에서 파일 경로가 될 수 없는 예약어들을 제거한다.
	 * @param sPath
	 * @return
	 */
	public static String stripReservedWord(String sPath) {

		// 예약어 제거하기
		sPath = sPath.replaceAll("(^|/|\\\\)"+"con"+"([.]|/|\\\\|$)", "$1"+"__con__"+"$2");
		sPath = sPath.replaceAll("(^|/|\\\\)"+"prn"+"([.]|/|\\\\|$)", "$1"+"__prn__"+"$2");
		sPath = sPath.replaceAll("(^|/|\\\\)"+"nul"+"([.]|/|\\\\|$)", "$1"+"__nul__"+"$2");
		sPath = sPath.replaceAll("(^|/|\\\\)"+"aux"+"([.]|/|\\\\|$)", "$1"+"__aux__"+"$2");
		
		return sPath;
	}

	/**
	 * 디렉토리 이름을 얻는 함수
	 * @param sPath
	 * @return
	 */
	public static String dirname(String sPath) {
		
		String sDir = "";
		
		int nPos = sPath.lastIndexOf(SEP_DIR);
		if(nPos != -1)
			sDir = sPath.substring(0, nPos+1);
		
		return sDir;
	}

	/**
	 * 확장자를 포함한 파일 이름을 얻는 함수
	 * @param sPath
	 * @return
	 */
	public static String filename(String sPath) {
		
		String sFile = "";
		
		int nPos = sPath.lastIndexOf(SEP_DIR);
		if(nPos != -1)
			sFile = sPath.substring(nPos+1);
		
		return sFile;
	}

	/**
	 * 확장자를 제외한 순수 파일 이름을 얻는 함수
	 * @param sPath
	 * @return
	 */
	public static String filebody(String sPath) {
		
		String sBody = "";
		
		sPath = filename(sPath);

		int nPos = sPath.lastIndexOf(".");
		if(nPos != -1)
			sBody = sPath.substring(0, nPos);
		else
			sBody = sPath;
		
		return sBody;
	}

	/**
	 * 파일의 확장자 이름을 얻는 함수
	 * @param path
	 * @return
	 */
	public static String fileext(String path) {
		
		String sExt = "";
		
		path = filename(path);

		int nPos = path.lastIndexOf(".");
		if(nPos != -1)
			sExt = path.substring(nPos+1);
		
		return sExt;
	}


	/**
	 * 파일의 이름을 바꿔주는 함수
	 * @param fileSrc
	 * @param fileDst
	 * @return
	 */
	public static boolean rename(String fileSrc, String fileDst) {
		
		File oFileSrc = new File(fileSrc);
		File oFileDst = new File(fileDst);
		return oFileSrc.renameTo(oFileDst);
	}
	
	/**
	 * 파일을 삭제하는 함수
	 * @param fileTarget
	 * @return
	 */
	public static boolean delete(String fileTarget) {
		
		File oFile = new File(fileTarget);
		return oFile.delete();
	}

	/**
	 * 디렉토리를 삭제하는 함수
	 * @param dirTarget
	 * @return
	 */
	public static boolean deleteDir(String dirTarget) {
		
		boolean bRes = true;
		
		ArrayList<String> lstFile = ZFile.listSubR2(dirTarget);
		for(String fileTarget: lstFile)
			bRes &= ZFile.delete(fileTarget);
		bRes &= delete(dirTarget);
		
		return bRes;
	}

	/**
	 * 새로운 디렉토리를 생성하는 함수
	 * @param dirTarget
	 * @return
	 */
	public static boolean mkdir(String dirTarget) {
		
		File oFile = new File(dirTarget);
		return oFile.mkdir();
	}

	/**
	 * 새로운 디렉토리를 자동으로 중간 디렉토리까지 생성하는 함수
	 * @param dirTarget
	 * @return
	 */
	public static boolean mkdirs(String dirTarget) {
		
		File oFile = new File(dirTarget);
		return oFile.mkdirs();
	}

	/**
	 * 파일을 이동하는 함수
	 * @param fileSrc
	 * @param fileDst
	 * @return
	 */
	public static boolean move(String fileSrc, String fileDst) {
		
		File oFileSrc = new File(fileSrc);
		File oFileDst = new File(fileDst);
		return oFileSrc.renameTo(oFileDst);
	}
	
	/**
	 * 
	 * @param fileTarget
	 * @return
	 */
	public static Long lineOf(String fileTarget) {
		
		return lineOf(fileTarget, DEF_CHARSET);
	}

	/**
	 * 
	 * @param fileTarget
	 * @param sCharset
	 * @return
	 */
	public static Long lineOf(String fileTarget, String sCharset) {
		
		long numLine = 0;
		
		ZFile zfInp = new ZFile(fileTarget, TO_READ, sCharset);

		try {
			for(int c=zfInp.oInpReader.read(); c!=-1; c=zfInp.oInpReader.read())
				if(c=='\n')
					numLine++;
		}
		catch(Exception e) {
		}

		zfInp.close();

		return numLine;
	}

	/**
	 * 파일의 전체 내용을 한번에 읽는 함수
	 * @param fileTarget
	 * @return
	 */
	public static String readAll(String fileTarget) {
		
		return readAll(fileTarget, DEF_CHARSET);
	}

	/**
	 * 파일의 전체 내용을 한번에 읽는 함수
	 * @param fileTarget
	 * @param sCharset
	 * @return
	 */
	public static String readAll(String fileTarget, String sCharset) {
		
		String sCont = null;

		// 파일 열기
		ZFile zfInp = new ZFile(fileTarget, TO_READ, sCharset);

		// 내용 읽기
		sCont = zfInp.readRemains();

		// 파일 닫기
		zfInp.close();

		return sCont;
	}
	
	/**
	 * 디렉토리 내 하위 항목들의 리스트를 리턴하는 함수
	 * @param dirInp
	 * @return
	 */
	public static ArrayList<String> listSub(String dirInp) {
		ArrayList<String> lstPath = new ArrayList<String>();
		for(String sFile : (new File(dirInp)).list())
			lstPath.add(dirInp+sFile);
		return lstPath;
	}
	
	/**
	 * 디렉토리 내 하위 항목들의 리스트를 리턴하는 함수 (Recursive) (Dir-First)
	 * @param dirInp
	 * @return
	 */
	public static ArrayList<String> listSubR(String dirInp) {
		ArrayList<String> lstPath = new ArrayList<String>();
		for(String sFile : (new File(dirInp)).list()) {
			lstPath.add(dirInp+sFile);
			if(new File(dirInp+sFile).isDirectory())
				lstPath.addAll(listSubR(dirInp+sFile));
		}
		return lstPath;
	}
	
	/**
	 * 디렉토리 내 하위 항목들의 리스트를 리턴하는 함수 (Recursive) (File-First)
	 * @param dirInp
	 * @return
	 */
	public static ArrayList<String> listSubR2(String dirInp) {
		ArrayList<String> lstPath = new ArrayList<String>();
		for(String sFile : (new File(dirInp)).list()) {
			if(new File(dirInp+sFile).isDirectory())
				lstPath.addAll(listSubR(dirInp+sFile));
			lstPath.add(dirInp+sFile);
		}
		return lstPath;
	}
	
	/**
	 * 디렉토리 내 하위 항목들의 개수를 리턴하는 함수
	 * @param dirInp
	 * @return
	 */
	public static int countSub(String dirInp) {
		return (new File(dirInp)).list().length;
	}
	
	/***************************************************************************
	* readObject : Desc
	***************************************************************************/
	public static Object readObject(String file)
	{
		Object ret = null;

		try {
			ObjectInput inp = new ObjectInputStream(new FileInputStream(file));
			ret = inp.readObject();
			inp.close();
		}
		catch(Exception e) {
		}

		return ret;
	}

	/***************************************************************************
	* truncate : 파일의 내용을 제거하는 함수
	***************************************************************************/
	public static boolean truncate(String filename)
	{
		boolean ret = false;

		ZFile.write(filename, "", false);
		if(ZFile.readAll(filename)==null || ZFile.readAll(filename).length()==0)
			ret = true;

		return ret;
	}

	/***************************************************************************
	* write : 파일에 버퍼의 내용을 쓰는 함수
	***************************************************************************/
	public static void write(String filename, String buf)
	{
		write(filename, buf, true);
	}

	/***************************************************************************
	* write : 파일에 버퍼의 내용을 쓰는 함수
	***************************************************************************/
	public static void write(String filename, String charset, String buf)
	{
		write(filename, charset, buf, true);
	}

	/***************************************************************************
	* write : 파일에 버퍼의 내용을 쓰는 함수
	*   + flag  = true(Append) / false
	***************************************************************************/
	public static void write(String filename, String buf, boolean flag)
	{
		write(filename, DEF_CHARSET, buf, flag);
	}

	/***************************************************************************
	* write : 파일에 버퍼의 내용을 쓰는 함수
	*   + flag  = true(Append) / false
	***************************************************************************/
	public static void write(String filename, String charset, String buf, boolean flag)
	{
		buf = buf.replaceAll(REG_LINE,"\n").replaceAll("\n",SEP_LINE);

		try {
			int type = (flag) ? TO_APPEND : TO_WRITE;
			ZFile out = new ZFile(filename, type, charset);
			out.write(buf);
			out.close();
		}
		catch(Exception e) {
		}
	}

	/***************************************************************************
	* writeLine : 파일에 한 줄의 내용을 쓰는 함수
	***************************************************************************/
	public static void writeLine(String filename, String buf)
	{
		write(filename, buf+SEP_LINE);
	}

	/***************************************************************************
	* writeLine : 파일에 한 줄의 내용을 쓰는 함수
	***************************************************************************/
	public static void writeLine(String filename, String charset, String buf)
	{
		write(filename, charset, buf+SEP_LINE);
	}

	/***************************************************************************
	* writeLine : 파일에 한 줄의 내용을 쓰는 함수
	*   + flag  = true(Append) / false
	***************************************************************************/
	public static void writeLine(String filename, String buf, boolean flag)
	{
		write(filename, buf+SEP_LINE, flag);
	}

	/***************************************************************************
	* writeLine : 파일에 한 줄의 내용을 쓰는 함수
	*   + flag  = true(Append) / false
	***************************************************************************/
	public static void writeLine(String filename, String charset, String buf, boolean flag)
	{
		write(filename, charset, buf+SEP_LINE, flag);
	}

	/***************************************************************************
	* writeObject : Desc
	***************************************************************************/
	public static void writeObject(String file, Object obj)
	{
		try {
			ObjectOutput out = new ObjectOutputStream(new FileOutputStream(file));
			out.writeObject(obj);
			out.close();
		}
		catch(Exception e) {
			if(ZFile.exists(file))
				ZFile.delete(file);
		}
	}
};
