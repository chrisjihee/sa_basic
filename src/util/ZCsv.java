/**
 * 
 */
package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

/**
 * CSV 파일을 처리하기 위한 클래스
 * 
 * @author Jihee Ryu
 */
public class ZCsv {
	
	public static final long serialVersionUID = 0x5000000000000102L;
	
	CsvReader oReader;
	CsvWriter oWriter;
	
	private ZCsv() {
		
	}
	
	public static ZCsv forReading(String fileInp) throws IOException {
		InputStreamReader oReader = new InputStreamReader(new FileInputStream(fileInp), "UTF-8");
		oReader.read(); // BOM을 읽어서 버림

		ZCsv oHandler = new ZCsv();
		oHandler.oReader = new CsvReader(oReader, ',');
		oHandler.oReader.readHeaders();
		oHandler.oReader.setSafetySwitch(false);
		return oHandler;
	}
	
	public static ZCsv forWriting(String fileOut, String[] aCol) throws IOException {
		OutputStreamWriter oWriter = new OutputStreamWriter(new FileOutputStream(fileOut), "UTF-8");
		oWriter.write(0xFEFF);

		ZCsv oHandler = new ZCsv();
		oHandler.oWriter = new CsvWriter(oWriter, ',');
		oHandler.oWriter.writeRecord(aCol);
		return oHandler;
	}
	
	public static ZCsv forAppending(String fileOut) throws IOException {
		OutputStreamWriter oWriter = new OutputStreamWriter(new FileOutputStream(fileOut, true), "UTF-8");

		ZCsv oHandler = new ZCsv();
		oHandler.oWriter = new CsvWriter(oWriter, ',');
		return oHandler;
	}
	
	public void write(Object[] aValue) throws IOException {
		oWriter.writeRecord(ZList.toStringArray(aValue));
	}
	
	public void flush() throws IOException {
		oWriter.flush();
	}
	
	public boolean read() throws IOException {
		return oReader.readRecord();
	}
	
	public String get(String sCol) throws IOException {
		return oReader.get(sCol);
	}
	
	public String get(int nCol) throws IOException {
		return oReader.get(nCol);
	}
	
	public void close() {
		if(oReader!=null)
			oReader.close();
		if(oWriter!=null)
			oWriter.close();
	}
}
