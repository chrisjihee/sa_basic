/**
 * 
 */
package util;

import java.io.FileInputStream;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.data.PointerUtils;
import net.didion.jwnl.dictionary.Dictionary;
import net.didion.jwnl.dictionary.MorphologicalProcessor;

/**
 * class of WordNet dictionary adapter using JWNL library
 * 
 * @author  Jihee Ryu
 * @since   2009/08/21
 * @version 2011/08/17
 */
public class WordNetDict {
	
	public static final int TYPE_WORD = 0x5001;
	public static final int TYPE_FIND = 0x5002;
	public static final int TYPE_SSID = 0x5003;
	
	public Dictionary oDicInstance;
	public PointerUtils oPointerUtils;
	public MorphologicalProcessor oMorphologicalProcessor;
	
	/**
	 * initialize WordNet dictionary
	 * @throws Exception
	 */
	public void init() throws Exception {
		
		this.init("rsc/wordnet/");
	}
	
	/**
	 * initialize WordNet dictionary
	 * @throws Exception
	 */
	public void init(String dirDict) throws Exception {
		
		JWNL.initialize(new FileInputStream(dirDict+"file_properties.xml"));
		oDicInstance = Dictionary.getInstance();
		oPointerUtils = PointerUtils.getInstance();
		oMorphologicalProcessor = oDicInstance.getMorphologicalProcessor();
	}
	
	/**
	 * close WordNet dictionary
	 */
	public void close() {
		
		JWNL.shutdown();
	}
	
	/**
	 * 
	 * @param sWord
	 * @return
	 * @throws JWNLException 
	 */
	public boolean isNoun(String sWord) throws JWNLException {

		return oDicInstance.lookupIndexWord(POS.NOUN, sWord)!=null;
	}
	
	/**
	 * 
	 * @param sVerb
	 * @return
	 * @throws JWNLException 
	 */
	public boolean isVerb(String sWord) throws JWNLException {

		return oDicInstance.lookupIndexWord(POS.VERB, sWord)!=null;
	}
	
	/**
	 * 
	 * @param sWord
	 * @return
	 * @throws JWNLException 
	 */
	public boolean isAdjective(String sWord) throws JWNLException {

		return oDicInstance.lookupIndexWord(POS.ADJECTIVE, sWord)!=null;
	}
	
	/**
	 * 
	 * @param sWord
	 * @return
	 * @throws JWNLException 
	 */
	public boolean isAdverb(String sWord) throws JWNLException {

		return oDicInstance.lookupIndexWord(POS.ADVERB, sWord)!=null;
	}

	/**
	 * 
	 * @param sWord
	 * @return
	 * @throws JWNLException 
	 */
	public boolean isRootVerb(String sWord) throws JWNLException {
		
		if(!isVerb(sWord))  return false;
		String sNorm = normalizeLex(POS.VERB, sWord.toLowerCase());
		return sNorm.equalsIgnoreCase(sWord);
	}

	/**
	 * normalize into base form of the given word
	 * @param sPos    a pos tag of the given word
	 * @param sWord   a given word
	 * @return
	 * @throws JWNLException 
	 * @throws Exception
	 */
	public String normalizeLex(POS oPos, String sWord) throws JWNLException {
		
		IndexWord oWord = oMorphologicalProcessor.lookupBaseForm(oPos, sWord);
		return oWord!=null ?oWord.getLemma() :sWord;
	}
}
