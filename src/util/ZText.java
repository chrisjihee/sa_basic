/**
 * 
 */
package util;

import java.util.Collection;
import java.util.HashMap;

/**
 * Text를 표현하고 처리하는 클래스
 * 
 * @author  Jihee Ryu
 * @since   2011/08/18
 * @version 2011/08/18
 */
public class ZText {
	
	public static final long serialVersionUID = 0x5000000000000407L;

	protected String sText = new String();

	/**
	 * Constructor function
	 */
	protected ZText() {
		
	}
	
	/**
	 * Constructor function
	 */
	protected ZText(Object oVal) {
		
		sText = oVal.toString();
	}
	
	/**
	 * Static constructor function
	 * @return
	 */
	public static ZText byNone() {
		
		return new ZText();
	}
	
	/**
	 * Static constructor function
	 * @param oVal
	 * @return
	 */
	public static ZText byObj(Object oVal) {
		
		return new ZText(oVal);
	}
	
	/**
	 * 
	 * @param lstObj
	 * @param sSep
	 * @return
	 */
	public static String implode(Object[] aObj, String sSep) {
		StringBuilder sbBuf = new StringBuilder();
		for(Object o : aObj) {
			if(sbBuf.length()>0)
				sbBuf.append(sSep);
			sbBuf.append(o==null? "null":o.toString());
		}
		return sbBuf.toString();
	}
	
	/**
	 * 
	 * @param lstObj
	 * @param sSep
	 * @return
	 */
	public static String implode(@SuppressWarnings("rawtypes") Collection lstObj, String sSep) {
		StringBuilder sbBuf = new StringBuilder();
		for(Object o : lstObj) {
			if(sbBuf.length()>0)
				sbBuf.append(sSep);
			sbBuf.append(o.toString());
		}
		return sbBuf.toString();
	}

	/**
	 * 
	 * @param sText
	 * @param sSep
	 * @param sSep2
	 * @return
	 */
	public static HashMap<String,String> splitAndPut(String sText, String sSep, String sSep2) {
	    HashMap<String,String> map = new HashMap<String,String>();
	    for(String sText2 : sText.split(sSep))
	    	map.put(sText2.split(sSep2,2)[0], sText2.split(sSep2,2)[1]);
	    return map;
	}

	/**
	 * 
	 * @param sText
	 * @param sSep
	 * @param sSep2
	 * @param sSep3
	 * @return
	 */
	public static HashMap<String,String> splitAndPut(String sText, String sSep, String sSep2, String sSep3) {
	    HashMap<String,String> map = new HashMap<String,String>();
	    for(String sText2 : sText.split(sSep))
    		for(String sKey : sText2.split(sSep2,2)[0].split(sSep3))
    			for(String sValue : sText2.split(sSep2,2)[1].split(sSep3))
    				map.put(sKey, sValue);
	    return map;
	}
}
