package test;

import java.io.FileInputStream;

import opennlp.tools.chunker.Chunker;
import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTagger;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class PhraseChunking {

	public static void main(String[] args) throws Exception {
		String text = "John loves ice cream cake. But he does not like orange juice.";
		SentenceDetector detector = new SentenceDetectorME(new SentenceModel(new FileInputStream("rsc/opennlp/en-sent.bin")));
		Tokenizer tokenizer = new TokenizerME(new TokenizerModel(new FileInputStream("rsc/opennlp/en-token.bin")));
		POSTagger tagger = new POSTaggerME(new POSModel(new FileInputStream("rsc/opennlp/en-pos-maxent.bin")));
		Chunker chunker = new ChunkerME(new ChunkerModel(new FileInputStream("rsc/opennlp/en-chunker.bin")));

		for(String sentence : detector.sentDetect(text)) {
		    String[] tokens = tokenizer.tokenize(sentence);
		    String[] tags = tagger.tag(tokens);
		    String[] chunks = chunker.chunk(tokens, tags);
		    System.out.println(toChunkString(tokens, tags, chunks));
		}
	}
	
	public static String toChunkString(String[] aTok, String[] aPos, String[] aCnk) {
		StringBuilder sbBuf = new StringBuilder();
		for(int i=0; i<aCnk.length; i++) {
			if(i>0 && !aCnk[i].startsWith("I-") && !aCnk[i-1].equals("O"))
				sbBuf.append(">");
			if(i>0 && aCnk[i].startsWith("B-"))
				sbBuf.append(" <"+aCnk[i].substring(2));
			else if(aCnk[i].startsWith("B-"))
				sbBuf.append("<"+aCnk[i].substring(2));
			sbBuf.append(" "+aTok[i]+"/"+aPos[i]);
		}
		if(!aCnk[aCnk.length-1].equals("O"))
			sbBuf.append(">");
		return sbBuf.toString().trim();
	}
}
