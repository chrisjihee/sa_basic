package test;

import java.io.StringReader;
import java.util.List;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class POSTagging {

	public static void main(String[] args) throws Exception {
		String text = "John loves Mary. She loves him too.";
		MaxentTagger tagger = new MaxentTagger(DefaultPaths.DEFAULT_POS_MODEL);

		for(List<HasWord> sentence : MaxentTagger.tokenizeText(new StringReader(text))) {
		    for(TaggedWord taggedWord : tagger.tagSentence(sentence))
		        System.out.print(taggedWord+" ");
		    System.out.println();
		}
	}
}
