package test;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.Options;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.trees.Tree;

public class ConstituencyParsing {

	public static void main(String[] args) throws Exception {
		String sentence = "John loves the ice cream cake on my table.";
		LexicalizedParser parser = LexicalizedParser.getParserFromFile(DefaultPaths.DEFAULT_PARSER_MODEL, new Options());

		Tree parseTree = parser.apply(sentence);
		System.out.println(parseTree);
	}
}
