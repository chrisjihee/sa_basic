package test;

import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class NamedEntityRecognition {

	public static void main(String[] args) throws Exception {
		String text = "John Brown bought 300 shares of Microsoft in Redmond at $5,000 in July, 2012.";
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		StanfordCoreNLP tool = new StanfordCoreNLP(props);
		Annotation annotation = new Annotation(text);
		tool.annotate(annotation);

		for(CoreMap sentence: annotation.get(SentencesAnnotation.class)) {
			for(CoreLabel token: sentence.get(TokensAnnotation.class)) {
				String tok = token.get(TextAnnotation.class);
				String ner = token.get(NamedEntityTagAnnotation.class);
				System.out.print(tok+"/"+ner+" ");
			}
			System.out.println();
		}
	}
}
