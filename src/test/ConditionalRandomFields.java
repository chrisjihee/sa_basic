package test;

import cc.mallet.fst.SimpleTagger;

public class ConditionalRandomFields {

	public static void main(String[] args) throws Exception {
		//SimpleTagger.main("--train true --model-file dat/crf/model.dat dat/crf/train.txt".split(" "));
		SimpleTagger.main("--model-file dat/crf/model.dat dat/crf/test.txt".split(" "));
	}
}
