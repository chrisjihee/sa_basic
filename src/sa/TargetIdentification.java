package sa;

import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.ZCsv;
import util.ZFile;
import util.ZText;
import cc.mallet.fst.SimpleTagger;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class TargetIdentification {
	
	/**
	 * 메인 프로그램
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// 문장 내 각 단어들에 대해 feature vector들을 생성하고 train 데이터, test 데이터, answer(test 데이터의 정답) 리스트를 출력한다.
		doFeatureGeneration("dat/ntcir/ntcir8-moat4.csv", "dat/ntcir/ntcir8-moat4-target.txt");
		
		/*
		 * [주의!] 다음의 CRF 모델 학습과 sequence 태깅은 동시에 실행될 수 없습니다. (둘 중 하나씩 실행하여야 합니다.)
		 */
		// train 데이터로 학습시킨 CRF 모델을 생성한다.
		doModelTraining("dat/ntcir/ntcir8-moat4-target(train).txt", "dat/ntcir/ntcir8-moat4-target.model");
		// 학습된 CRF 모델로 test 데이터 각 단어에 대해 Target 여부를 sequence 태깅한다.
		doEstimation("dat/ntcir/ntcir8-moat4-target.model", "dat/ntcir/ntcir8-moat4-target(test).txt", "dat/ntcir/ntcir8-moat4-target(estimate).txt");
		
		// test 데이터에 대해 태깅된 결과가 실제 정답 데이터와 일치 여부를 기준으로 CRF모델의 성능을 평가한다.
		doResultEvaluation("dat/ntcir/ntcir8-moat4-target(answer).txt", "dat/ntcir/ntcir8-moat4-target(estimate).txt");
		
		System.out.println("All Done!");
	}
	
	/**
	 * 문장 내 각 단어들에 대해 feature vector들을 생성하고 train 데이터, test 데이터, answer 리스트를 출력하는 모듈 
	 * @param fileInp
	 * @param fileOut
	 * @throws Exception
	 */
	public static void doFeatureGeneration(String fileInp, String fileOut) throws Exception {
		MaxentTagger tagger = new MaxentTagger(DefaultPaths.DEFAULT_POS_MODEL);

		ZFile fOut1 = new ZFile(fileOut.replace(".txt", "(train).txt"), ZFile.TO_WRITE);
		ZFile fOut2 = new ZFile(fileOut.replace(".txt", "(test).txt"), ZFile.TO_WRITE);
		ZFile fOut3 = new ZFile(fileOut.replace(".txt", "(answer).txt"), ZFile.TO_WRITE);
		ZCsv fInp = ZCsv.forReading(fileInp);

		int nCnt = 0;
		int nInstance = 0;
		while(fInp.read()) {
			if(++nCnt%100==0)
				System.out.println(" + [Sent] "+nCnt);
			String sOpinionated = fInp.get("Opinionated");
			if(sOpinionated.equals("N"))
				continue;
			String sSent = fInp.get("Sent");
			String sSentPos = fInp.get("POS(Sent)");
			
			// Holder Mark 배열에 Holder 위치 표시하기
			String sHolder = fInp.get("Holder");
			String[] aHolderMark = new String[sSentPos.split(" ").length];
			Arrays.fill(aHolderMark, "Holder=X");
			for(String sUnit : sHolder.split("<AND>")) {
				sUnit = sUnit.replaceAll("\\(.+\\)", "");
				for(String sUnit2 : sUnit.split(":|\\|"))
					if(sUnit2.length()>0 && Pattern.compile("\\b"+Pattern.quote(sUnit2)+"\\b").matcher(sSent).find()) {
						String sUnitPos = tagPos(tagger, sUnit2);
						ArrayList<String> lstPatternWord = new ArrayList<String>();
						for(String sTaggedWord : sUnitPos.split(" ")) {
							String sWord = sTaggedWord.split("/")[0];
							lstPatternWord.add(Pattern.quote(sWord)+"/[^ ]+");
						}
						Matcher mUnitPos = Pattern.compile(ZText.implode(lstPatternWord, " ")).matcher(sSentPos);
						while(mUnitPos.find()) {
							int idxUnitPos = sSentPos.substring(0, mUnitPos.start()).trim().split(" ").length;
							for(int i=0; i<sUnitPos.split(" ").length; i++)
								aHolderMark[idxUnitPos+i] = "Holder=O";
						}
					}
			}
			if(!Arrays.asList(aHolderMark).contains("Holder=O")) // Holder가 있는 것에 대해서만 처리
				continue;

			// Target Mark 배열에 Holder 위치 표시하기
			String sTarget = fInp.get("Target");
			String[] aTargetMark = new String[sSentPos.split(" ").length];
			Arrays.fill(aTargetMark, "Target=X");
			for(String sUnit : sTarget.split("<AND>")) {
				sUnit = sUnit.replaceAll("\\(.+\\)", "");
				for(String sUnit2 : sUnit.split(":|\\|"))
					if(sUnit2.length()>0 && Pattern.compile("\\b"+Pattern.quote(sUnit2)+"\\b").matcher(sSent).find()) {
						String sUnitPos = tagPos(tagger, sUnit2);
						ArrayList<String> lstPatternWord = new ArrayList<String>();
						for(String sTaggedWord : sUnitPos.split(" ")) {
							String sWord = sTaggedWord.split("/")[0];
							lstPatternWord.add(Pattern.quote(sWord)+"/[^ ]+");
						}
						Matcher mUnitPos = Pattern.compile(ZText.implode(lstPatternWord, " ")).matcher(sSentPos);
						while(mUnitPos.find()) {
							int idxUnitPos = sSentPos.substring(0, mUnitPos.start()).trim().split(" ").length;
							for(int i=0; i<sUnitPos.split(" ").length; i++)
								aTargetMark[idxUnitPos+i] = "Target=O";
						}
					}
			}
			if(!Arrays.asList(aTargetMark).contains("Target=O")) // Target이 있는 것에 대해서만 처리
				continue;
			
			// feature vector를 50:50으로 Train 데이터와 Test로 나눠서 출력함
			boolean bTrain = (++nInstance)%2==1 ?true :false;
			int idxWord = 0;
			for(String sTaggedWord : sSentPos.split(" ")) {
				String sWord = sTaggedWord.split("/")[0];
				String sPos = sTaggedWord.split("/")[1];
				if(bTrain)
					fOut1.writeLine(sWord+"  "+sPos+"  "+aHolderMark[idxWord]+"  "+aTargetMark[idxWord]); // train 데이터 출력
				if(!bTrain) {
					fOut2.writeLine(sWord+"  "+sPos+"  "+aHolderMark[idxWord]); // test 데이터 출력
					fOut3.writeLine(aTargetMark[idxWord]); // answer 리스트 출력
				}
				idxWord++;
			}
			
			// 한 문장 처리 후 공백 라인을 출력함으로 각 문장을 구분 시킴
			if(bTrain)
				fOut1.writeLine("");
			if(!bTrain) {
				fOut2.writeLine("");
				fOut3.writeLine("");
			}
		}
		
		fInp.close();
		fOut1.close();
		fOut2.close();
		fOut3.close();
	}
	
	/**
	 * 주어진 한 문장에 대해 POS태깅된 결과를 구하고 리턴하는 함수
	 * @param tagger
	 * @param sSent
	 * @return
	 */
	private static String tagPos(MaxentTagger tagger, String sSent) {
		ArrayList<String> lstTag = new ArrayList<String>();
		ArrayList<String> lstSent = new ArrayList<String>();
		for(List<HasWord> sentence : MaxentTagger.tokenizeText(new StringReader(sSent))) {
		    for(TaggedWord taggedWord : tagger.tagSentence(sentence))
		    	lstTag.add(taggedWord.toString());
		    lstSent.add(ZText.implode(lstTag, " "));
		}
		return ZText.implode(lstSent, "<AND>");
	}
	
	/**
	 * train 데이터로 학습시킨 CRF 모델을 생성하는 모듈
	 * @param fileTrain
	 * @param fileModel
	 * @throws Exception
	 */
	public static void doModelTraining(String fileTrain, String fileModel) throws Exception {
		String sCommandArgs = "--train true --model-file "+fileModel+" "+fileTrain;
		SimpleTagger.main(sCommandArgs.split(" "));
	}
	
	/**
	 * 학습된 CRF 모델로 주어진 데이터에 sequence 태깅하는 모듈
	 * @param fileModel
	 * @param fileTest
	 * @param fileOut
	 * @throws Exception
	 */
	public static void doEstimation(String fileModel, String fileTest, String fileOut) throws Exception {
		String sCommandArgs = "--model-file "+fileModel+" "+fileTest;
		PrintStream consol = System.out;
		System.setOut(new PrintStream(fileOut));
		SimpleTagger.main(sCommandArgs.split(" "));
		System.setOut(consol);
	}
	
	/**
	 * CRF모델의 성능을 평가하는 모듈
	 * @param fileAns
	 * @param fileEst
	 */
	public static void doResultEvaluation(String fileAns, String fileEst) {
		ZFile fAns = new ZFile(fileAns, ZFile.TO_READ);
		ZFile fEst = new ZFile(fileEst, ZFile.TO_READ);
		
		HashMap<String,Integer> mapCount = new HashMap<String,Integer>();
		for(String sType : "OO,OX,XO,XX".split(","))
			mapCount.put(sType, 0);
		
		while(true) {
			String sAns = fAns.readLine();
			String sEst = fEst.readLine();
			if(sAns==null || fEst==null)
				break;
			
			sAns = sAns.trim().replace("Target=", ""); // 정답 데이터
			sEst = sEst.trim().replace("Target=", ""); // 추측 데이터 (CRF 모델에 의한)
			if(!sAns.matches("[OX]") || !sEst.matches("[OX]"))
				continue;

			String sType = sAns+sEst; // 정답과 추측을 붙여서 어떤 식의 결과인지 구분함 (OO 또는 OX 또는 XO 또는 XX)
			mapCount.put(sType, mapCount.get(sType)+1); // 4가지 결과(OO, OX, XO, XX) 종류에 대하여 카운트함
		}
		fAns.close();
		fEst.close();
		
		// 4가지 결과 종류에 대한 카운트 출력
		for(String sType : "OO,OX,XO,XX".split(","))
			System.out.println(sType+" : "+mapCount.get(sType));
		
		// 성능 수치(Precision, Recall, F1) 계산하여 출력
		double fPrec = mapCount.get("OO").doubleValue() / (mapCount.get("OO")+mapCount.get("XO"));
		double fRec = mapCount.get("OO").doubleValue() / (mapCount.get("OO")+mapCount.get("OX"));
		double fF1 = (2*fPrec*fRec) / (fPrec+fRec);
		System.out.println("Precision = "+fPrec);
		System.out.println("Recall = "+fRec);
		System.out.println("F1 = "+fF1);
	}
}
