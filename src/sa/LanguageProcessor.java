package sa;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.POS;
import util.WordNetDict;
import util.ZCsv;
import util.ZText;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.parser.lexparser.Options;
import edu.stanford.nlp.pipeline.DefaultPaths;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreePrint;

public class LanguageProcessor {
	
	/**
	 * 메인 프로그램
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// 한 문장에 대해 여러 Polarity가 있는 경우에 대해서 합산한다.
		doPolarityVoting("dat/ntcir/ntcir8-moat.csv", "dat/ntcir/ntcir8-moat2.csv");
		
		// 각 문장을 POS태깅으로 언어처리하여 출력한다.
		doPosTagging("dat/ntcir/ntcir8-moat2.csv", "dat/ntcir/ntcir8-moat3.csv");
		// 문장 내 명사, 동사, 형용사, 부사 단어를 WordNet 사전에 등록된 표제어 Normalize하여 출력한다.
		doWordNormalizing("dat/ntcir/ntcir8-moat3.csv", "dat/ntcir/ntcir8-moat4.csv");
		// 각 문장을 Dependency파싱으로 언어처리하여 출력한다.
		//doDepTagging("dat/ntcir/ntcir8-moat4.csv", "dat/ntcir/ntcir8-moat5.csv");
		
		System.out.println("All Done!");
	}
	
	/**
	 * Polarity를 투표하여 합산하는 모듈
	 * @param fileInp
	 * @param fileOut
	 * @throws IOException
	 */
	public static void doPolarityVoting(String fileInp, String fileOut) throws IOException {
		ZCsv fInp = ZCsv.forReading(fileInp);
		
		HashMap<String,Integer> mapNumVote = new HashMap<String,Integer>();
		HashMap<String,Integer> mapPolarityValue = new HashMap<String,Integer>();
		HashMap<String,ArrayList<String>> mapHolder = new HashMap<String,ArrayList<String>>();
		HashMap<String,ArrayList<String>> mapTarget = new HashMap<String,ArrayList<String>>();
		while(fInp.read()) {
			String sTopicID = fInp.get("TopicID");
			String sDocID = fInp.get("DocID");
			String sSentNo = fInp.get("SentNo");
			String sSentID = sTopicID+"_"+sDocID+"_"+sSentNo;
			String sPolarity = fInp.get("Polarity");
			String sHolder = fInp.get("Holder");
			String sTarget = fInp.get("Target");
			
			if(!mapNumVote.containsKey(sSentID))
				mapNumVote.put(sSentID, 0);
			if(!sPolarity.equals(""))
				mapNumVote.put(sSentID, mapNumVote.get(sSentID)+1);
			
			// 긍정과 부정에 대해서 값을 합산하기
			if(!mapPolarityValue.containsKey(sSentID))
				mapPolarityValue.put(sSentID, 0);
			if(sPolarity.equals("NEG"))
				mapPolarityValue.put(sSentID, mapPolarityValue.get(sSentID)-1);
			if(sPolarity.equals("POS"))
				mapPolarityValue.put(sSentID, mapPolarityValue.get(sSentID)+1);
			
			// 같은 문장에 태깅된 복수개의 Holder를 누적하기
			if(!mapHolder.containsKey(sSentID))
				mapHolder.put(sSentID, new ArrayList<String>());
			if(!sHolder.trim().equals(""))
				mapHolder.get(sSentID).add(sHolder.trim());
			
			// 같은 문장에 태깅된 복수개의 Target을 누적하기
			if(!mapTarget.containsKey(sSentID))
				mapTarget.put(sSentID, new ArrayList<String>());
			if(!sTarget.trim().equals(""))
				mapTarget.get(sSentID).add(sTarget.trim());
		}
		fInp.close();
		
		// 각 문장에 대해서 합쳐진 속성 값들(Polarity, Holder, Target)을 가져와서 출력하기 
		fInp = ZCsv.forReading(fileInp);
		ZCsv fOut = ZCsv.forWriting(fileOut, "TopicID,DocID,SentNo,Sent,Opinionated,Holder,Target,Relevant,Polarity".split(","));
		String sPrevSentID = "n/a";
		while(fInp.read()) {
			String sTopicID = fInp.get("TopicID");
			String sDocID = fInp.get("DocID");
			String sSentNo = fInp.get("SentNo");
			String sSentID = sTopicID+"_"+sDocID+"_"+sSentNo;
			if(sPrevSentID.equals(sSentID))
				continue;
			
			String sSent = fInp.get("Sent");
			String sOpinionated = fInp.get("Opinionated");
			String sHolder = ZText.implode(mapHolder.get(sSentID), "<AND>");
			String sTarget = ZText.implode(mapTarget.get(sSentID), "<AND>");
			String sRelevant = fInp.get("Relevant");
			String sPolarity = "";
			if(mapNumVote.get(sSentID)>0)
				sPolarity = mapPolarityValue.get(sSentID)>0 ?"POS" :mapPolarityValue.get(sSentID)<0 ?"NEG" :"NEU";
			
			fOut.write(new Object[]{sTopicID, sDocID, sSentNo, sSent, sOpinionated, sHolder, sTarget, sRelevant, sPolarity});
			sPrevSentID = sSentID;
		}
		fInp.close();
		fOut.close();
	}
	
	/**
	 * 파일 내에서의 모든 문장을 POS태깅 언어처리 후 출력하는 모듈
	 * @param fileInp
	 * @param fileOut
	 * @throws Exception
	 */
	public static void doPosTagging(String fileInp, String fileOut) throws Exception {
		MaxentTagger tagger = new MaxentTagger(DefaultPaths.DEFAULT_POS_MODEL);
		
		ZCsv fInp = ZCsv.forReading(fileInp);
		ZCsv fOut = ZCsv.forWriting(fileOut, "TopicID,DocID,SentNo,Sent,POS(Sent),Opinionated,Holder,Target,Relevant,Polarity".split(","));
		
		while(fInp.read()) {
			String sTopicID = fInp.get("TopicID");
			String sDocID = fInp.get("DocID");
			String sSentNo = fInp.get("SentNo");
			String sSent = fInp.get("Sent");
			String sSentPos = tagPos(tagger, sSent); // POS태깅
			String sOpinionated = fInp.get("Opinionated");
			String sHolder = fInp.get("Holder");
			String sTarget = fInp.get("Target");
			String sRelevant = fInp.get("Relevant");
			String sPolarity = fInp.get("Polarity");
			
			fOut.write(new Object[]{sTopicID, sDocID, sSentNo, sSent, sSentPos, sOpinionated, sHolder, sTarget, sRelevant, sPolarity});
		}
		
		fInp.close();
		fOut.close();
	}
	
	/**
	 * 주어진 한 문장에 대해 POS태깅된 결과를 구하고 리턴하는 함수
	 * @param tagger
	 * @param sSent
	 * @return
	 */
	private static String tagPos(MaxentTagger tagger, String sSent) {
		ArrayList<String> lstTag = new ArrayList<String>();
		ArrayList<String> lstSent = new ArrayList<String>();
		for(List<HasWord> sentence : MaxentTagger.tokenizeText(new StringReader(sSent))) {
		    for(TaggedWord taggedWord : tagger.tagSentence(sentence))
		    	lstTag.add(taggedWord.toString());
		    lstSent.add(ZText.implode(lstTag, " "));
		}
		return ZText.implode(lstSent, "<AND>"); // 여러 문장인 경우 "<AND>"로 연결하여 리턴함
	}
	
	/**
	 * 파일 내에서의 모든 문장을 WordNet 단어 Normalizing 언어처리 후 출력하는 모듈
	 * @param fileInp
	 * @param fileOut
	 * @throws Exception
	 */
	public static void doWordNormalizing(String fileInp, String fileOut) throws Exception {
		WordNetDict dict = new WordNetDict();
		dict.init();
		
		ZCsv fInp = ZCsv.forReading(fileInp);
		ZCsv fOut = ZCsv.forWriting(fileOut, "TopicID,DocID,SentNo,Sent,POS(Sent),KEY(Sent),Opinionated,Holder,Target,Relevant,Polarity".split(","));
		
		int nCnt = 0;
		while(fInp.read()) {
			if(++nCnt%100==0)
				System.out.println(" + [Sent] "+nCnt);
			String sTopicID = fInp.get("TopicID");
			String sDocID = fInp.get("DocID");
			String sSentNo = fInp.get("SentNo");
			String sSent = fInp.get("Sent");
			String sSentPos = fInp.get("POS(Sent)");
			String sSentKey = normalizeWord(dict, sSentPos); // WordNet 단어 Normalizing
			String sOpinionated = fInp.get("Opinionated");
			String sHolder = fInp.get("Holder");
			String sTarget = fInp.get("Target");
			String sRelevant = fInp.get("Relevant");
			String sPolarity = fInp.get("Polarity");
			
			fOut.write(new Object[]{sTopicID, sDocID, sSentNo, sSent, sSentPos, sSentKey, sOpinionated, sHolder, sTarget, sRelevant, sPolarity});
		}
		
		fInp.close();
		fOut.close();
	}
	
	/**
	 * POS태깅된 문장을 POS태그를 보고 명사, 동사, 형용사, 부사에 대해 Normalizing하여 리턴하는 함수
	 * @param dict
	 * @param sSentPos
	 * @return
	 * @throws JWNLException
	 */
	private static String normalizeWord(WordNetDict dict, String sSentPos) throws JWNLException {
		ArrayList<String> lstKeyword = new ArrayList<String>();
		for(String sTaggedWord : sSentPos.split("\\s+")) {
			String sWord = sTaggedWord.split("/")[0].toLowerCase();
			String sPos = sTaggedWord.split("/")[1];
			String sKeyword = null;
			if(sPos.startsWith("NN")) { // 명사
				sWord = dict.normalizeLex(POS.NOUN, sWord);
				sKeyword = sWord+"/NN";
			}
			else if(sPos.startsWith("VB")) { // 동사
				sWord = dict.normalizeLex(POS.VERB, sWord);
				sKeyword = sWord+"/VB";
			}
			else if(sPos.startsWith("JJ")) { // 형용사
				sWord = dict.normalizeLex(POS.ADJECTIVE, sWord);
				sKeyword = sWord+"/JJ";
			}
			else if(sPos.startsWith("RB")) { // 부사
				sWord = dict.normalizeLex(POS.ADVERB, sWord);
				sKeyword = sWord+"/RB";
			}
			if(sKeyword!=null)
				lstKeyword.add(sKeyword);
		}
		
		return ZText.implode(lstKeyword, " "); // Normalize된 명사, 동사, 형용사, 부사를 스페이스로 붙여 리턴함 
	}
	
	/**
	 * 파일 내에서의 모든 문장을 Dependency 파싱 언어처리 후 출력하는 모듈
	 * @param fileInp
	 * @param fileOut
	 * @throws Exception
	 */
	public static void doDepTagging(String fileInp, String fileOut) throws Exception {
		LexicalizedParser parser = LexicalizedParser.getParserFromFile(DefaultPaths.DEFAULT_PARSER_MODEL, new Options());
		
		ZCsv fInp = ZCsv.forReading(fileInp);
		ZCsv fOut = ZCsv.forWriting(fileOut, "TopicID,DocID,SentNo,Sent,POS(Sent),DEP(Sent),Opinionated,Holder,Target,Relevant,Polarity".split(","));
		
		int nCnt = 0;
		while(fInp.read()) {
			if(++nCnt%10==0)
				System.out.println(" + [Sent] "+nCnt);
			String sTopicID = fInp.get("TopicID");
			String sDocID = fInp.get("DocID");
			String sSentNo = fInp.get("SentNo");
			String sSent = fInp.get("Sent");
			String sSentPos = fInp.get("POS(Sent)");
			String sSentKey = fInp.get("KEY(Sent)");
			String sSentDep = tagDep(parser, sSent).replace("\n", "<AND>"); // Dependency 파싱
			String sOpinionated = fInp.get("Opinionated");
			String sHolder = fInp.get("Holder");
			String sTarget = fInp.get("Target");
			String sRelevant = fInp.get("Relevant");
			String sPolarity = fInp.get("Polarity");
			
			fOut.write(new Object[]{sTopicID, sDocID, sSentNo, sSent, sSentPos, sSentKey, sSentDep, sOpinionated, sHolder, sTarget, sRelevant, sPolarity});
		}
		
		fInp.close();
		fOut.close();
	}
	
	/**
	 * 주어진 한 문장에 대해 Dependency 파싱된 결과를 구하고 리턴하는 함수
	 * @param parser
	 * @param sSent
	 * @return
	 */
	private static String tagDep(LexicalizedParser parser, String sSent) {
		Tree parseTree = parser.apply(sSent);
		return printTree(parseTree, "typedDependencies"); // 파싱 트리를 typedDependencies 형태로 구하여 리턴함
	}
	
	/**
	 * 파싱 트리를 지정된 포맷으로 출력하는 함수
	 * @param tree
	 * @param format
	 * @return
	 */
	private static String printTree(Tree tree, String format) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		TreePrint oTreePrint = new TreePrint(format);
		oTreePrint.printTree(tree, pw);
		return sw.toString().trim();
	}
}
