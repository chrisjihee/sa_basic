package sa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import util.WordNetDict;
import util.ZCsv;
import util.ZFile;
import util.ZText;

public class PolarityJudgment {
	
	/**
	 * 메인 프로그램
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// 언어처리된 문장 리스트에서 긍정, 부정, 중립 키워드들의 TFIDF를 통한 가중치를 계산한다.
		doKeywordExtraction("dat/ntcir/ntcir8-moat4.csv", "dat/ntcir/ntcir8-moat4-polarity_keyword.csv");
		
		// 각 문장에 내에서 특정 조건을 만족하는 키워드들의 count를 feature로 사용함으로 각 문장을 feature vector로 변환하고 CSV와 ARFF 포맷으로 출력한다. 
		doFeatureGeneration("dat/ntcir/ntcir8-moat4-polarity_keyword.csv", "dat/ntcir/ntcir8-moat4.csv", "dat/ntcir/ntcir8-moat4-polarity.csv", "dat/ntcir/ntcir8-moat4-polarity.arff");
		
		System.out.println("All Done!");
	}
	
	/**
	 * 각 Polarity Class별(긍정, 부정, 중립) 키워드들과 각 키워드들의 가중치를 계산하는 모듈
	 * @param fileInp
	 * @param fileOut
	 * @throws Exception
	 */
	public static void doKeywordExtraction(String fileInp, String fileOut) throws Exception {
		// WordNet 모듈 초기화하기
		WordNetDict dict = new WordNetDict();
		dict.init();
		
		HashSet<String> setKeyword = new HashSet<String>();
		HashMap<String,ArrayList<String>> mapKeyword = new HashMap<String,ArrayList<String>>();
		for(String sClass : "POS,NEG,NEU".split(","))
			mapKeyword.put(sClass, new ArrayList<String>());
		
		ZCsv fInp = ZCsv.forReading(fileInp);
		while(fInp.read()) {
			String sSentKey = fInp.get("KEY(Sent)");
			String sPolarity = fInp.get("Polarity");
			if(sPolarity.equals(""))
				continue;
			
			// 각 Class별 Keyword 등록하기 및 전체 Keyword 집합 등록하기
			for(String sKeyword : sSentKey.split("\\s+")) {
				mapKeyword.get(sPolarity).add(sKeyword);
				setKeyword.add(sKeyword);
			}
		}
		fInp.close();
		
		// 출력할 컬럼명 리스트 생성하기
		ArrayList<String> lstColumn = new ArrayList<String>();
		lstColumn.add("Keyword");
		lstColumn.add("DF");
		for(String sClass : "POS,NEG,NEU".split(","))
			lstColumn.add("TF_"+sClass);
		for(String sClass : "POS,NEG,NEU".split(","))
			lstColumn.add("TFIDF_"+sClass);
		for(String sClass : "POS,NEG,NEU".split(","))
			lstColumn.add("TFIDFR_"+sClass);
		
		ZCsv fOut = ZCsv.forWriting(fileOut, lstColumn.toArray(new String[0]));
		for(String sKeyword : setKeyword) {
			// 출력값 리스트 선언하고 Keyword 추가하기
			ArrayList<Object> lstValue = new ArrayList<Object>();
			lstValue.add(sKeyword);
			
			// DF 계산하기
			Integer nDF = 0;
			for(String sClass : "POS,NEG,NEU".split(",")) {
				String sClassCont = ZText.implode(mapKeyword.get(sClass), " ");
				Matcher mKeyword = Pattern.compile("\\b"+Pattern.quote(sKeyword)+"\\b").matcher(sClassCont);
				if(mKeyword.find())
					nDF++;
			}
			lstValue.add(nDF);
			if(nDF == 0)
				continue;
			
			// Polarity Class별로 TF 계산하기
			HashMap<String,Integer> mapTF = new HashMap<String,Integer>();
			for(String sClass : "POS,NEG,NEU".split(",")) {
				String sClassCont = ZText.implode(mapKeyword.get(sClass), " ");
				Matcher mKeyword = Pattern.compile("\\b"+Pattern.quote(sKeyword)+"\\b").matcher(sClassCont);
				int nTF = 0;
				while(mKeyword.find())
					nTF++;
				mapTF.put(sClass, nTF);
				lstValue.add(nTF);
			}
			
			// Polarity Class별로 TFIDF 계산하기
			HashMap<String,Double> mapTFIDF = new HashMap<String,Double>();
			for(String sClass : "POS,NEG,NEU".split(",")) {
				double fTFIDF = mapTF.get(sClass).doubleValue() / Math.pow(nDF,4);
				mapTFIDF.put(sClass, fTFIDF);
				lstValue.add(fTFIDF);
			}
			
			// 평균 TFIDF 계산하기
			double fTFIDF_Avg = 0;
			for(String sClass : "POS,NEG,NEU".split(","))
				fTFIDF_Avg += mapTFIDF.get(sClass);
			fTFIDF_Avg /= "POS,NEG,NEU".split(",").length;

			// Polarity Class별 TFIDF 비율 계산하기
			for(String sClass : "POS,NEG,NEU".split(",")) {
				double fTFIDF = mapTFIDF.get(sClass);
				double fTFIDF_Rate = Math.log(1+fTFIDF) * fTFIDF/fTFIDF_Avg;
				lstValue.add(fTFIDF_Rate);
			}
			
			// 계산된 값 출력하기
			fOut.write(lstValue.toArray());
		}
		fOut.close();
	}
	
	/**
	 * 각 문장을 feature vector로 변환하여 출력하는 모듈
	 * @param fileFeat
	 * @param fileInp
	 * @param fileOut
	 * @param fileOut2
	 * @throws IOException
	 */
	public static void doFeatureGeneration(String fileFeat, String fileInp, String fileOut, String fileOut2) throws IOException {
		// feature로 사용할 Keyword 선정하기
		ArrayList<String> lstKeywordFeature = new ArrayList<String>();
		ZCsv fFeat = ZCsv.forReading(fileFeat);
		while(fFeat.read()) {
			String sKeyword = fFeat.get("Keyword");
			double fPosScore = Double.parseDouble(fFeat.get("TFIDFR_POS"));
			double fNegScore = Double.parseDouble(fFeat.get("TFIDFR_NEG"));
			double fNeuScore = Double.parseDouble(fFeat.get("TFIDFR_NEU"));
			// 긍정, 부정, 중립의 TFIDF 비율이 3.0 이상인 키워드들을 선별하여 리스트에 추가함
			if(fPosScore>3.0 || fNegScore>3.0 || fNeuScore>3.0)
				lstKeywordFeature.add(sKeyword);
		}
		fFeat.close();
		
		// 출력할 컬럼명 리스트 생성하기
		ArrayList<String> lstColumn = new ArrayList<String>();
		lstColumn.add("SentID");
		lstColumn.add("Sent");
		for(String sKeywordFeature : lstKeywordFeature)
			lstColumn.add("TF_"+sKeywordFeature);
		lstColumn.add("Polarity");
		
		ZCsv fOut = ZCsv.forWriting(fileOut, lstColumn.toArray(new String[0]));
		ZCsv fInp = ZCsv.forReading(fileInp);
		
		// ARFF 파일 헤더 출력하기
		ZFile fOut2 = new ZFile(fileOut2, ZFile.TO_WRITE);
		fOut2.writeLine("@RELATION sentiment");
		fOut2.writeLine("");
		for(String sKeywordFeature : lstKeywordFeature)
			fOut2.writeLine("@ATTRIBUTE "+"TF_"+sKeywordFeature.replaceAll("[,']","")+" REAL");
		fOut2.writeLine("@ATTRIBUTE class {POS,NEG,NEU}");
		fOut2.writeLine("");

		// ARFF 파일 DATA 섹션 출력하기
		int nCnt = 0;
		fOut2.writeLine("@DATA");
		while(fInp.read()) {
			if(++nCnt%100==0)
				System.out.println(" + [Sent] "+nCnt);
			String sTopicID = fInp.get("TopicID");
			String sDocID = fInp.get("DocID");
			String sSentNo = fInp.get("SentNo");
			String sSentID = sTopicID+"_"+sDocID+"_"+sSentNo;
			String sSent = fInp.get("Sent");
			String sSentKey = fInp.get("KEY(Sent)");
			String sPolarity = fInp.get("Polarity");
			if(sPolarity.equals(""))
				continue;
			
			ArrayList<Object> lstFeatureValue = new ArrayList<Object>();
			lstFeatureValue.add(sSentID);
			lstFeatureValue.add(sSent);
			
			for(String sKeywordFeature : lstKeywordFeature) {
				Matcher mKeyword = Pattern.compile("\\b"+Pattern.quote(sKeywordFeature)+"\\b").matcher(sSentKey);
				int nTF = 0;
				while(mKeyword.find())
					nTF++;
				lstFeatureValue.add(nTF);
			}

			lstFeatureValue.add(sPolarity);
			fOut.write(lstFeatureValue.toArray());
			fOut2.writeLine(ZText.implode(lstFeatureValue.subList(2, lstFeatureValue.size()), ","));
		}
		
		fInp.close();
		fOut.close();
		fOut2.close();
	}
}
